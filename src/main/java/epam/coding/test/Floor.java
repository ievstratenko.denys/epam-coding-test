package epam.coding.test;


import java.util.ArrayList;
import java.util.List;

import static epam.coding.test.OfficeSpaceСhallenge.CUBES_FOR_TEAM;

class Floor {
	private static int counter = 0;

	private Integer cubesCount;
	private Integer id;
	private List<Project> projects = new ArrayList<>();

	Floor(Integer cubesCount) {
		this.id = ++counter;
		this.cubesCount = cubesCount;
	}

	private Integer freeCubesCount() {
		return cubesCount - projects.stream().mapToInt(Project::getSize).sum() * CUBES_FOR_TEAM;
	}

	boolean placeProject(Project p) {
		if (freeCubesCount() >= p.cubesRequired()) {
			projects.add(p);
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Floor{" +
				"id=" + id +
				", projects=" + projects +
				'}';
	}
}