package epam.coding.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OfficeSpaceСhallenge {

	static int CUBES_FOR_TEAM = 2;

	public static void main(String[] args) {
		System.out.println("args " + Arrays.toString(args));

		Integer cubesCount = Integer.valueOf(extractArg(args, 'c'));
		System.out.println("cubesCount " + cubesCount);

		List<Integer> projectsSizes = getProjectsSizes(extractArg(args, 'p'));
		System.out.println("projectsSizes " + projectsSizes);

		List<Project> projects = projectsSizes.stream()
				.map(Project::new)
				.sorted(Comparator.reverseOrder())
				.collect(Collectors.toList());
		System.out.println("Projects: " + projects);

		List<Floor> floors = new ArrayList<>();
		projects.forEach(p -> placeProject(floors, p, cubesCount));

		System.out.println("\n RESULT:");
		floors.forEach(System.out::println);
	}

	private static void placeProject(List<Floor> floors, Project p, Integer cubesCount) {
		for (Floor f : floors) {
			if (f.placeProject(p)) {
				return;
			}
		}
		Floor newFloor = new Floor(cubesCount);
		if (newFloor.placeProject(p)) {
			floors.add(newFloor);
		} else {
			// TODO cases with too big projects
			System.out.println("Too big project to be placed at a single floor: " + p);
		}
	}

	private static List<Integer> getProjectsSizes(String projectsSizesStr) {
		List<Integer> projectsSizes = null;
		try {
			projectsSizes = Stream.of(projectsSizesStr.split(","))
					.map(Integer::valueOf)
					.collect(Collectors.toList());
		} catch (Exception e) {
			System.out.println("Check format of " + projectsSizesStr + ". Comma should be used as separator");
		}
		return projectsSizes;
	}

	private static String extractArg(String[] args, char prefixChar) {
		String prefix = "-" + prefixChar;
		List<String> requiredArgs = Stream.of(args).filter(arg -> arg.startsWith(prefix)).collect(Collectors.toList());
		if (requiredArgs.isEmpty()) {
			throw new IllegalStateException("No arguments with " + prefix + " prefix");
		}
		if (requiredArgs.size() > 1) {
			throw new IllegalStateException("More than one argument with " + prefix + " prefix");
		}
		return requiredArgs.get(0).replaceAll(prefix, "");
	}
}
