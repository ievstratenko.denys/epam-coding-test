package epam.coding.test;


import static epam.coding.test.OfficeSpaceСhallenge.CUBES_FOR_TEAM;

class Project implements Comparable<Project> {
	private static int counter = 0;

	private Integer id;
	private Integer size;

	Project(Integer size) {
		this.id = ++counter;
		this.size = size;
	}

	@Override
	public String toString() {
		return "Project{" +
				"id=" + id +
				", size=" + size +
				'}';
	}

	Integer getSize() {
		return size;
	}

	@Override
	public int compareTo(Project other) {
		return size.compareTo(other.size);
	}

	int cubesRequired() {
		return size * CUBES_FOR_TEAM;
	}
}
